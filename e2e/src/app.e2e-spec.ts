import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
    page.navigateTo();
  });

  it('should load without error', () => {
  });

  it('should default to selecting the first element in the list', () => {
    expect(page.videoList.getSelectedVideo()).toBe(page.videoList.getVideoId(0));
  });

  it('should be able to select an item in the list', () => {
    page.videoList.selectVideoByIndex(1);
    expect(page.videoList.getVideoId(1)).toBe(page.videoList.getSelectedVideo());
  });

  it('should be able to add a video', () => {
    const numVideos = page.videoList.getNumVideos();
    page.videoList.addVideo();
    expect(page.videoList.getNumVideos()).toBe(numVideos.then(x => x + 1));
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    }));
  });
});

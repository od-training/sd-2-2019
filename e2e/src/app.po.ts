import { browser, by, element } from 'protractor';
import { VideoListPO } from './video-list.po';

export class AppPage {
  videoList = new VideoListPO(element(by.css('app-video-list')));
  navigateTo() {
    return browser.get('/') as Promise<any>;
  }
}

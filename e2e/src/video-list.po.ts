import { ElementFinder, by } from 'protractor';

export class VideoListPO {
  constructor(private element: ElementFinder) { }
  getNumVideos() {
    return this.element.all(by.css('li')).count();
  }
  getSelectedVideo() {
    return this.element.$('.selected').getAttribute('id');
  }
  getVideo(index: number) {
    return this.element.all(by.css('li')).get(index);
  }
  getVideoId(index: number) {
    return this.getVideo(index).getAttribute('id');
  }
  selectVideoByIndex(index: number) {
    return this.getVideo(index).click();
  }

  selectVideoById(id: string) {
    return this.element.$('[id="' + id + '"]').click();
  }

  addVideo() {
    return this.element.$('app-add-video button').click();
  }
}

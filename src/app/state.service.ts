import { Injectable } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { View, Video, UnsavedVideo } from './types';
import { map, startWith, switchMap } from 'rxjs/operators';
import { Observable, combineLatest } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { AppState, TryToAddVideo } from './state';


function isSenior(v: View) {
  return v.age > 60;
}

function isMinor(v: View) {
  return v.age < 18;
}
// It is encouraged to extract complex functions for unit testing
const filterViews = ([details, filter]) => details
    .filter(v => v.region === filter.region || filter.region === 'All')
    .filter(v => filter.seniors ? true : !isSenior(v))
    .filter(v => filter.minors ? true : !isMinor(v));

const selectedVideoQP = 'videoid';

@Injectable({
  providedIn: 'root'
})
export class StateService {
  videos = this.store.pipe(select(s => s.videos));
  selectedVideo: Observable<Video> = this.ar.queryParams.pipe(
    map(qp => qp[selectedVideoQP]),
    switchMap(id => this.videos.pipe(map(videos => videos.find(v => v.id === id))))
    );
  filter = this.fb.group({
    region: ['All'],
    fromDate: ['1995-01-01'],
    toDate: ['2019-01-01'],
    minors: [true],
    youngAdults: [true],
    adults: [true],
    seniors: [true]
  });
  filteredViews: Observable<View[]> = combineLatest(
    this.selectedVideo.pipe(map(v => v ? v.viewDetails : [])),
    this.filter.valueChanges
      .pipe(
        startWith(this.filter.value)
      )
  ).pipe(map(filterViews));
  constructor(
    private fb: FormBuilder,
    private ar: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>) { }

  setSelectedVideo(v: Video) {
    this.router.navigate([], { queryParams: { [selectedVideoQP]: v.id }, queryParamsHandling: 'merge' });
  }

  initializeSelectedVideo(vl: Video[]) {
    if (!this.ar.snapshot.queryParams[selectedVideoQP]) {
      this.setSelectedVideo(vl[0]);
    }
  }

  addVideo(newVideo: UnsavedVideo) {
    this.store.dispatch(new TryToAddVideo(newVideo));
  }

}

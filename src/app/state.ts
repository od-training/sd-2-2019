import { Action } from '@ngrx/store';
import { Video, UnsavedVideo } from './types';
import { Filter } from './dashboard/view-filter/view-filter.component';

export interface AppState {
  videos: Video[];
  recentErrors: string[];
}

export const getVideoList = 'GET_VIDEO_LIST';
export class GetVideoList implements Action {
  type = getVideoList;
}

const videoListArrived = 'VIDEO_LIST_ARRIVED';
export class VideoListArrived implements Action {
  type = videoListArrived;
  constructor(readonly list: Video[]) { }
}

export const tryToAddVideo = 'TRY_TO_ADD_VIDEO';
export class TryToAddVideo {
  type = tryToAddVideo;
  constructor(readonly video: UnsavedVideo) { }
}

const videoAdded = 'VIDEO_ADDED';
export class VideoAdded implements Action {
  type = videoAdded;
  constructor(readonly video: Video) { }
}

export const deleteVideo = 'DELETE_VIDEO';
export class DeleteVideo implements Action {
  type = deleteVideo;
  constructor(readonly videoId: string) { }
}

export const deleteVideoSuccess = 'DELETE_VIDEO_SUCCESS';
export class DeleteVideoSucces implements Action {
  type = deleteVideoSuccess;
  constructor(readonly videoId: string) { }
}

export const deleteVideoFailure = 'DELETE_VIDEO_FAILURE';
export class DeleteVideoFailure implements Action {
  type = deleteVideoFailure;
  constructor(readonly videoId: string) { }
}

const filterChange = 'FILTER_CHANGE';
export class FilterChange implements Action {
  type = filterChange;
  constructor(readonly newFilter: Filter) { }
}

export function videoListReducer(prevList: Video[] = [], action: Action) {
  switch (action.type) {
    case videoListArrived:
      return (action as VideoListArrived).list;
    case videoAdded:
      // Don't do this
      // prevList.push((action as VideoAdded).video);
      // return prevList;
      return [...prevList, (action as VideoAdded).video];
    case deleteVideoSuccess:
      return prevList.filter(v => v.id !== (action as DeleteVideoSucces).videoId);
    default:
      return prevList;
  }
}


export function recentErrorsReducer(prev: string[] = [], action: Action) {
  switch (action.type) {
    case deleteVideoFailure:
      return [...prev, 'Failed to delete video ' + (action as DeleteVideoFailure).videoId];
    default:
      return prev;
  }
}

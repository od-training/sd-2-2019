import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Video, UnsavedVideo } from './types';
import { throwError } from 'rxjs';

const url = 'http://localhost:8000/api/videos';
@Injectable({
  providedIn: 'root'
})
export class VideoService {

  constructor(private http: HttpClient) { }

  getVideos() {
    return this.http.get<Video[]>(url);
  }

  addVideo(v: UnsavedVideo) {
    return this.http.post<Video>(url, v);
  }

  deleteVideo(id: string) {
    if (Math.random() > .25) {
      return this.http.delete(url + id);
    } else {
      return throwError('Failed');
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState, GetVideoList } from 'src/app/state';

@Component({
  selector: 'app-refresh-button',
  templateUrl: './refresh-button.component.html',
  styleUrls: ['./refresh-button.component.css']
})
export class RefreshButtonComponent implements OnInit {

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
  }

  refresh() {
    this.store.dispatch(new GetVideoList());
  }

}

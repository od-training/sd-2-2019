import { Component, OnInit, Input } from '@angular/core';
import { View } from 'src/app/types';

@Component({
  selector: 'app-view-details',
  templateUrl: './view-details.component.html',
  styleUrls: ['./view-details.component.css']
})
export class ViewDetailsComponent implements OnInit {
  @Input() views: View[];
  constructor() { }

  ngOnInit() {
  }

}

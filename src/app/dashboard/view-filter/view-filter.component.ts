import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

export interface Filter {
  'region': string;
  'fromDate': string;
  'toDate':  string;
  'minors': boolean;
  'youngAdults': boolean;
  'adults': boolean;
  'seniors': boolean;
}

@Component({
  selector: 'app-view-filter',
  templateUrl: './view-filter.component.html',
  styleUrls: ['./view-filter.component.css']
})
export class ViewFilterComponent implements OnInit {
  @Input() filterForm: FormGroup;
  regions = [
    'All',
    'North America',
    'Europe',
    'Asia'
  ];
  ageGroups = [
    {
      id: 'minors',
      displayName: 'Under 18'
    },
    {
      id: 'youngAdults',
      displayName: '18 - 40'
    },
    {
      id: 'adults',
      displayName: '40 - 60'
    },
    {
      id: 'seniors',
      displayName: 'Over 60'
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}

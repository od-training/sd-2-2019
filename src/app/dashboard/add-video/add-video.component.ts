import { Component, OnInit } from '@angular/core';
import { StateService } from 'src/app/state.service';

@Component({
  selector: 'app-add-video',
  templateUrl: './add-video.component.html',
  styleUrls: ['./add-video.component.css']
})
export class AddVideoComponent implements OnInit {

  constructor(private state: StateService) { }

  ngOnInit() {
  }

  addVideo() {
    this.state.addVideo({
      title: `Video ${Math.floor(Math.random() * 10000)}`,
      author: 'A Robot',
      viewDetails: []
    });
  }

}

import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState, DeleteVideo } from 'src/app/state';

@Component({
  selector: 'app-delete-video',
  templateUrl: './delete-video.component.html',
  styleUrls: ['./delete-video.component.css']
})
export class DeleteVideoComponent implements OnInit {
  @Input() videoId: string;
  constructor(private store: Store<AppState>) { }

  ngOnInit() {
  }

  delete() {
    this.store.dispatch(new DeleteVideo(this.videoId));
    return false;
  }

}

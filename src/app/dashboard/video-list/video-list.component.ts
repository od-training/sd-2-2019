import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Video } from 'src/app/types';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent implements OnInit {
  @Input() videos: Video[];
  @Input() selectedVideo: Video;
  @Output() videoSelected = new EventEmitter<Video>();
  constructor() { }

  ngOnInit() {
  }

  selectVideo(v: Video) {
    this.videoSelected.emit(v);
  }

}

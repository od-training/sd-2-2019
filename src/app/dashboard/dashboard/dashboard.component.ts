import { Component, OnInit } from '@angular/core';

import { StateService } from 'src/app/state.service';
import { AppState } from 'src/app/state';
import { Store, select } from '@ngrx/store';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  recentErrors = this.store.pipe(select(s => s.recentErrors));

  constructor(public state: StateService, private store: Store<AppState>) { }

  ngOnInit() {
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { VideoListComponent } from './video-list/video-list.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { VideoEmbedderComponent } from './video-embedder/video-embedder.component';
import { ViewDetailsComponent } from './view-details/view-details.component';
import { ViewFilterComponent } from './view-filter/view-filter.component';
import { AddVideoComponent } from './add-video/add-video.component';
import { RefreshButtonComponent } from './refresh-button/refresh-button.component';
import { DeleteVideoComponent } from './delete-video/delete-video.component';

@NgModule({
  declarations: [VideoListComponent, DashboardComponent, VideoEmbedderComponent, ViewDetailsComponent, ViewFilterComponent, AddVideoComponent, RefreshButtonComponent, DeleteVideoComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      { path: '', component: DashboardComponent }
    ])
  ]
})
export class DashboardModule { }

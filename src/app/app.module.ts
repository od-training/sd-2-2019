import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { AppState, videoListReducer, recentErrorsReducer } from './state';
import { EffectsModule } from '@ngrx/effects';
import { EffectsService } from './effects.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    StoreModule.forRoot<AppState>({
      videos: videoListReducer,
      recentErrors: recentErrorsReducer
    }),
    EffectsModule.forRoot([EffectsService])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { videoListReducer, VideoListArrived, VideoAdded, DeleteVideoSucces } from './state';
import { Video } from './types';

fdescribe('The video list reducer', () => {
  it('Ignores other random events', () => {
    const previousState = [];
    expect(videoListReducer(previousState, { type: 'asdfasdf' })).toBe(previousState);
  });

  it('defaults to an empty array', () => {
    expect(videoListReducer(undefined, { type: 'init'})).toEqual([]);
  });

  it('updates when the videos arrive', () => {
    const prev = [],
      newData: Video[] = [{
        author: 'asd0',
        id: 'asd',
        title: 'asd',
        viewDetails: []
      }];
      expect(videoListReducer(prev, new VideoListArrived(newData))).toBe(newData);
  });

  it('adds a video to the list', () => {
    const prev = [],
      newData: Video = {
        author: 'asd0',
        id: 'asd',
        title: 'asd',
        viewDetails: []
      };
      expect(videoListReducer(prev, new VideoAdded(newData))).toEqual([newData]);
  });

  it('can remove a video from the list', () => {
    const prev: Video[] = [{
        author: 'asd0',
        id: 'asd',
        title: 'asd',
        viewDetails: []
      }];
      expect(videoListReducer(prev, new DeleteVideoSucces(prev[0].id))).toEqual([]);
  });
});

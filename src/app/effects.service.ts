import { Injectable } from '@angular/core';
import { Actions, Effect, ofType, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { getVideoList, VideoListArrived, GetVideoList, tryToAddVideo,
  TryToAddVideo, VideoAdded, deleteVideo, DeleteVideo, DeleteVideoSucces, DeleteVideoFailure } from './state';
import { switchMap, map, tap, concatMap, catchError } from 'rxjs/operators';
import { VideoService } from './video.service';
import { StateService } from './state.service';
import { interval, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EffectsService {

  @Effect()
  loadList = this.actions.pipe(
    ofType(getVideoList),
    switchMap(() => this.vs.getVideos()),
    tap(videos => this.state.initializeSelectedVideo(videos)),
    map(videos => new VideoListArrived(videos))
  );

  @Effect()
  initialize = this.actions.pipe(
    ofType(ROOT_EFFECTS_INIT),
    map(() => new GetVideoList())
  );

  @Effect()
  addVideo = this.actions.pipe(
    ofType(tryToAddVideo),
    concatMap((a: TryToAddVideo) => this.vs.addVideo(a.video)),
    map(v => new VideoAdded(v))
  );

  // @Effect()
  // refresh = interval(30 * 1000)
  //   .pipe(map(() => new GetVideoList()));

  @Effect()
  deleteVideo = this.actions.pipe(
    ofType(deleteVideo),
    concatMap((a: DeleteVideo) =>
      this.vs.deleteVideo(a.videoId)
        .pipe(
          map(() => new DeleteVideoSucces(a.videoId)),
          catchError(() => of(new DeleteVideoFailure(a.videoId)))
        )
    )
  );

  constructor(private actions: Actions, private vs: VideoService, private state: StateService) { }
}
